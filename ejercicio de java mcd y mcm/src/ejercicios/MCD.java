package ejercicios;

public class MCD {

	 
	    public static void main (String[] Args) {
	        System.out.println("�Cu�l es el mcd de 4 y 12? Es "+obtener_mcd(4,12));
	        System.out.println("�Cu�l es el mcd de 8 y 12? Es "+obtener_mcd(8,12));
	        System.out.println("�Cu�l es el mcd de 4 y 17? Es "+obtener_mcd(4,17));
	        System.out.println("�Cu�l es el mcd de 96 y 36? Es "+obtener_mcd(96,36));
	       
	    }
	   
	    static int obtener_mcd(int a, int b) {
	       if(b==0)
	           return a;
	       else
	           return obtener_mcd(b, a % b);
	   }
	}
