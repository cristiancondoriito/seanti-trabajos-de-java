package ejercicios;
import java.util.Scanner;
public class MinComMult {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
//pedimos los numeros al usuario
		System.out.println("ingrese el primer numero");
		int num1 = scan.nextInt();
		System.out.println("ingrese el segundo numero");
		int num2 = scan.nextInt();
//selecionamos el menor entre num1 y num2
		int min = Math.min(num1, num2);
//inicializamos la variable mcm que sera la que almacenara el resultado final
		int mcm = 0;
//Inicializamos el ciclo for a tarves del cual se realizaran las iteracciones
		for (int i=1; i<=min; i++) {
//realizamos la condicion para saber si el modulo de num1 y num2 
//entre cada uno de los numeros enteros positivos menores o igual al menor de los dos 	
// es equivalente a 0 y de ser lo procedemos a realizar el proceso
			if (num1%i==0 && num2%i==0) {
//se asigna como mcd a cada uno de los numeros que cumpla
//con la concicion indicada . sin embargo ,el maximo comun divisor
//sera solamente el ultimo numeroque las cumpla antes de finalizar el ciclo
			int mcd = i;
//se calcula el minimo comun multiplo
		mcm = (num1*num2)/mcd;
		
			}
		}
//se muestra el resultado en pantalla
		System.out.println("el resultado del M.C.M entre " +num1+" y " +num2+"es:"+mcm);
	}

}
